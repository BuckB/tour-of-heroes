import { InMemoryDbService } from 'angular-in-memory-web-api';

export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const heroes = [
      { id: 11, nome: 'Rick and Morty' },
      { id: 12, nome: 'Draxx' },
      { id: 13, nome: 'Obi-Wan Kenobi' },
      { id: 14, nome: 'Asterix' },
      { id: 15, nome: 'John Snow' },
      { id: 16, nome: 'Rocket Raccoon' },
      { id: 17, nome: 'Deadpool' },
      { id: 18, nome: 'Sherlock Holmes' },
      { id: 19, nome: 'Conan' },
      { id: 20, nome: 'Wolverine' }
    ];
    return {heroes};
  }
}
